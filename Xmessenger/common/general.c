//
//  general.c
//  Xmessenger
//
//  Created by iremux on 05.09.2020.
//  Copyright © 2020 Роман Паньшин. All rights reserved.
//


#include "general.h"

int sendMessage(int csock, char* message) //функция отправки сообщения
{
    char ans[MESSAGE_SIZE];                    //буфер сообщения
    memset(&ans, 0, sizeof(ans));                   //обнуляем буфер
    strcpy(ans, message);
     if(send(csock, ans, MESSAGE_SIZE,0) < 0)        //отсылаем серверу
    {
        printf("%sНе удалось отправить данные: %s%s\n",RED, strerror(errno),RESET);
        return -1;
    }
    return 0;
}
