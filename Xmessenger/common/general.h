#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <netdb.h>
#include <sys/poll.h>
#include <unistd.h>

//проверка, задан ли шаблон INADDR_NONE, который обозначает сразу все доступные сетевые интерфейсы
//на некоторых платформах, он может быть не задан.
#ifndef INADDR_NONE
#define INADDR_NONE 0xfffffffff
#endif

#define MESSAGE_SIZE 256 // размер сообщения
#define RESET   "\033[0m"
#define YELLOW  "\033[1;33m"
#define RED     "\033[1;31m"
#define GREEN   "\x1B[32m"   //цвета для сообщений



//extern int errno;     //глобальная переменная, которая хранит код последней ошибки

int sendMessage(int csock, char* message); //int sendMessage(int csock, char* message); //функция отправки сообщения
