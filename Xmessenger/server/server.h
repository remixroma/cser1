#define CLIENT_QUANTITY 50

//функция создания и связывания сокета. объявление
//аргументы:
//port - порт, с которым связывается сервер
//transport - протокол, по которому будет работать сервер (tcp или udp)
//qlen - длина  очереди
int sock(const char *port, const char *transport, int qlen);
char *nameEdditor(char name[MESSAGE_SIZE]);  //функция редактирует имя
char *nameAdder(char name[MESSAGE_SIZE], char message[MESSAGE_SIZE]); //функция добавляет имя в сообщение

struct clientsdata
{
    int csock;
    char name[MESSAGE_SIZE];
};
