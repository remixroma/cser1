/************************************************/
/*    client.c - TCP/IP клиент мессенджера      */
/*  Паньшин Роман 26.07.2020                    */
/*  Версия 1.4.1 open chat                      */
/************************************************/

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/select.h>
#include <sys/poll.h>
#include <unistd.h>

int extern errno;     //глобальная переменная, которая хранит код последней ошибки

//функция подключения к серверу. объявление
//аргументы:
//host - адрес (имя) сервера
//port - порт сервера
//transport - протокол tcp или udp
int connectsock(const char *host, const char *port, const char *transport);

#define MESSAGE_SIZE 256 //размер строки
#define RESET   "\033[0m"
#define YELLOW  "\033[1;33m"
#define RED     "\033[1;31m"
#define GREEN   "\x1B[32m"   //цвета для сообщений

int sendMessage(int csock, char* message)
{
    if(send(csock, message, MESSAGE_SIZE, 0) < 0)        //отсылаем серверу
    {
        printf("Не удалось отправить данные: %s\n", strerror(errno));
        return -1;
    }

    return 0;
}

int receiveMessage(int csock, char* message)
{
    char ans[MESSAGE_SIZE];
    if(recv(csock, ans, MESSAGE_SIZE, 0) < 0)
    {
        printf("Не удалось отправить данные серверу: %s\n", strerror(errno));
        return -1;
    }
    if(strncmp(ans, "exit", 4) == 0)
    {
        printf("%s", ans);
        usleep(3);
        return 1;
    }
    fputs(ans, stdout);
    return 0;
}


//главная функция
int main(int argc, char **argv)
{
    system("clear");
    int sock;     //сокет
    char msg[MESSAGE_SIZE];
    char ans[MESSAGE_SIZE];    //буфер сообщения
    struct pollfd fds[1];  //структура для проверки сокета
    struct pollfd mypoll = {STDIN_FILENO, POLLIN|POLLPRI}; //структура для проверки потока ввода
    printf("Введите имя: ");
    scanf("%s", msg);
    printf("\n");

    sock = connectsock("95.217.211.124", "3123", "tcp");
    sendMessage(sock, msg);//отправляем имя
    memset(msg,'\0', MESSAGE_SIZE);
    fds[0].fd = sock;
    fds[0].events = POLLIN; //ожидаем отправки с сервера
    
        if(sock < 0)    //проверяем дескриптор сокета
            return -1;
        else         //подключились
            {
                printf("%sУстановлено соединение%s \n",GREEN, RESET);
                printf("Вы можете отправлять сообщения\n");
                
                while (1)
                {
                    fflush(stdout); //очищаем поток вывода
                    fflush(stdin); //очищаем аотоко ввода
                    if (poll(&mypoll, 1, 2) )
                    {
                        mypoll.revents = 0; //
                        fgets(msg, MESSAGE_SIZE, stdin);
                        sendMessage(sock, msg);
                    }
                        
                    int ret = poll(fds, 1, 1); // проверяем пришло ли изменнение
                    if ( ret == -1 ) //ошибка
                    {
                        printf("%sОшибка проверки сокета: %s%s", RED, strerror(errno), RESET);
                        return -1;
                    }
                    else if ( ret == 0 ) //тайм-оут
                        usleep(1);
                    else //пришло
                    {
                        if(fds[0].revents & POLLIN)
                        {
                            fds[0].revents = 0;
                            if((receiveMessage(sock, ans))) //если  receiveMessage возвращает один, выходим, тк пришел "end"
                            {
                                shutdown(sock, 2);     //закрываем сокет
                                return 0;
                            }
                        }
                    }
                }
            //shutdown(sock,2);     //закрываем сокет
            }
    return 0;
    }

//функция подключения к серверу. реализация
int connectsock(const char *host, const char *port, const char *transport)
{
    struct hostent *phe; //указатель на запись с информацией о хосте
    //struct servent *pse; //указатель на запись с информацией о службе
    struct protoent *ppe; //указатель на запись с информацией о протоколе
    struct sockaddr_in sin; //структура IP-адреса оконечной точки
    int s, type; //дескриптор сокета и тип сокета

    //обнуляем структуру адреса
    memset(&sin, 0, sizeof(sin));
    //указываем тип адреса (IPv4)
    sin.sin_family = AF_INET;
    //задаем порт
    sin.sin_port = htons((unsigned short)atoi(port));
    //преобразовываем имя хоста в IP-адрес, предусмотрев возможность представить его
    //в точечном десятичном формате
    if(phe = gethostbyname(host))
        memcpy(&sin.sin_addr, phe->h_addr, phe->h_length);
    //преобразовываем имя транспортного протокола в номер протокола
    if((ppe = getprotobyname(transport)) == 0)
        {
            printf("%sОшибка преобразования имени транспортного протокола: %s%s\n",RED, strerror(errno),RESET);    //в случае неудачи выводим сообщение ошибки
            return -1;
        }
    //используем имя протокола для определения типа сокета
    if(strcmp(transport, "udp") == 0)
        type = SOCK_DGRAM;
    else
        type = SOCK_STREAM;
    //создание сокета
    s = socket(PF_INET, type, ppe->p_proto);
    if(s < 0)
        {
            printf("%s Ошибка создания сокета: %s%s\n",RED, strerror(errno), RESET);    //в случае неудачи выводим сообщение ошибки
            return -1;
        }
    //попытка подключить сокет
    if(connect(s, (struct sockaddr *)&sin, sizeof(sin)) < 0)
        {
            printf("%s Не удалось подключится к серверу: %s%s\n",RED, strerror(errno),RESET);    //в случае неудачи выводим сообщение ошибки
            return -1;
        }
    //возвращаем дескриптор подключенного сокета
    return s;
}





