/************************************************/
/*	client.c - TCP/IP клиент мессенджера        */
/*  Паньшин Роман 24.06.2020                    */
/*  Версия 1.3.1 color update                   */
/************************************************/

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

extern errno; 	//глобальная переменная, которая хранит код последней ошибки

//функция подключения к серверу. объявление
//аргументы:
//host - адрес (имя) сервера
//port - порт сервера
//transport - протокол tcp или udp
int connectsock(const char *host, const char *port, const char *transport);
const int MESSAGE_SIZE=21;

#define RESET   "\033[0m"
#define YELLOW  "\033[1;33m"
#define RED     "\033[1;31m"
#define GREEN   "\x1B[32m"   //цвета для сообщений

int sendMessage(int csock, char* message)
{
 	if(write(csock, message, sizeof(message)) < 0)		//отсылаем серверу
	{
		printf("Не удалось отправить данные: %s\n", strerror(errno));
		return -1;
	}     
	return 0;                     
}

int receiveMessage(int csock, char* message)
{
	char ans[MESSAGE_SIZE];	
	if(read(csock, ans, sizeof(ans)) < 0)
	{
		printf("Не удалось отправить данные серверу: %s\n", strerror(errno));
		return -1;
	}
	printf(ans);
	return 0;
}


//главная функция
int main(int argc, char **argv)
{
	int sock; 	//сокет
	char msg[MESSAGE_SIZE];
	char ans[MESSAGE_SIZE];	//буфер сообщения
		sock = connectsock("95.217.211.124", "3123", "tcp");
		if(sock < 0)	//проверяем дескриптор сокета
			return -1;
		else 		//подключились
			{
				printf("%sУстановлено соединение%s \n",GREEN, RESET);
				//отправляем имя
				printf("Введите имя: ");
				scanf("%s",&msg);
				printf("\n");
				write(sock, msg, sizeof(msg));
				memset(msg,'\0',MESSAGE_SIZE);
				
				while (1)
				{
					printf("Вы: ");
					if (scanf("%s",&msg))
					{
						sendMessage(sock, msg);
						if(strcmp(msg,"end") == 0)
							{
								close(sock);	//закрываем сокет
								return 0;
							}
					}
						
					
					if(receiveMessage(sock, &ans) == 0)
					{
						if(strcmp(ans,"end") == 0)
						{
							close(sock);	//закрываем сокет
							return 0;
						}
					}
				printf("\n");	
				}
			close(sock);	//закрываем сокет
			}
	return 0;
	}

//функция подключения к серверу. реализация
int connectsock(const char *host, const char *port, const char *transport)
{
	struct hostent *phe; //указатель на запись с информацией о хосте
	struct servent *pse; //указатель на запись с информацией о службе
	struct protoent *ppe; //указатель на запись с информацией о протоколе
	struct sockaddr_in sin; //структура IP-адреса оконечной точки 
	int s, type; //дескриптор сокета и тип сокета

	//обнуляем структуру адреса
	memset(&sin, 0, sizeof(sin));
	//указываем тип адреса (IPv4) 
	sin.sin_family = AF_INET;
	//задаем порт
	sin.sin_port = htons((unsigned short)atoi(port)); 	
	//преобразовываем имя хоста в IP-адрес, предусмотрев возможность представить его
	//в точечном десятичном формате
	if(phe = gethostbyname(host))
		memcpy(&sin.sin_addr, phe->h_addr, phe->h_length);
	//преобразовываем имя транспортного протокола в номер протокола
	if((ppe = getprotobyname(transport)) == 0)
		{
			printf("%sОшибка преобразования имени транспортного протокола: %s%s\n",RED, strerror(errno),RESET);	//в случае неудачи выводим сообщение ошибки
			return -1;			
		}	
	//используем имя протокола для определения типа сокета 	 
	if(strcmp(transport, "udp") == 0)
		type = SOCK_DGRAM;
	else
		type = SOCK_STREAM;			
	//создание сокета
	s = socket(PF_INET, type, ppe->p_proto);
	if(s < 0)
		{
			printf("%s Ошибка создания сокета: %s%s\n",RED, strerror(errno), RESET);	//в случае неудачи выводим сообщение ошибки
			return -1;
		}
	//попытка подключить сокет
	if(connect(s, (struct sockaddr *)&sin, sizeof(sin)) < 0)
		{
			printf("%s Не удалось подключится к серверу: %s%s\n",RED, strerror(errno),RESET);	//в случае неудачи выводим сообщение ошибки
			return -1;			
		}
	//возвращаем дескриптор подключенного сокета
	return s;
}

