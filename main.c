/*Крестики нолики*/

#include <stdio.h>

char a[9] ={ "1" "2" "3" "4" "5" "6" "7" "8" "9"};
int result = 0;

int checkmate_kr(char c)
{
    if ( (a[0] == a[4]) && (a[8]==c) && (a[4]==a[8]) )
    {
        return 1;       /*по первой диагонали */
    }
    if ( (a[2] == a[4]) && (a[4]==c) && (a[2]==a[6]) )
    {
        return 1;       /*по второй диагонали */
    }
    if ( (a[0] == a[3]) && (a[6]==c) && (a[6]==a[0]) )
    {
        return 1;      /*по первому стобцу*/
    }
    if ( (a[0] == a[1]) && (a[1]==c) && (a[2]==a[0]) )
    {
        return 1;     /*по первой горизонтали */
    }
    if ( (a[1] == a[4]) && (a[1]==c) && (a[7]==a[1]) )
    {
        return 1;  /*по второму столбцу */
    }
    if ( (a[2] == a[5]) && (a[2]==c) && (a[8]==a[2]) )
    {
        return 1; /*по третьему столбцу */
    }
    if ( (a[3] == a[4]) && (a[4]==c) && (a[3]==a[5]) )
    {
        return 1;   /*по второй горизонтали */
    }
    if ( (a[6] == a[7]) && (a[7]==c) && (a[8]==a[6]) )
    {
        return 1;   /*по третей горизонтали */
    }
    return 0;
}

int vibor()
{
    int z =0;
    printf("\nДобро пожаловать в крестики нолики!\n");
    printf("\nВыберите режим игры\n1)Два игрока\n2)Игрок против компьютера\n3)Выйти\n\n");
    scanf("%d",&z);
    return (z);
}

int vivod(void)
{
    printf("\n");
    printf("%c %c %c\n",a[0],a[1],a[2]);
    printf("%c %c %c\n",a[3],a[4],a[5]);
    printf("%c %c %c\n",a[6],a[7],a[8]);
    printf("\n");
    return 0;
}

int krstep(void)
{
    vivod();
    int step=0;
    printf("Ход крестиков\n\n");
    scanf("%d", &step);
    step = step - 1;
    if (a[step] == 'x')
    {
        printf("Невозможный ход\n\n");
        krstep();
    }
    else
        if (a[step] == 'o')
        {
            printf("Невозможный ход\n\n");
            krstep();
        }
        else
        {
            a[step] = 'x';
            vivod();
            
        }
    return 0;
}

int nullstep(void)
{
    int step=0;
    printf("Ход ноликов\n\n");
    scanf("%d", &step);
    step = step - 1;
    if (a[step] == 'x')
    {
        printf("Невозможный ход\n\n");
        vivod();
        nullstep();
    }
    else
        if (a[step] == 'o')
        {
            printf("Невозможный ход\n\n");
            vivod();
            nullstep();
        }
        else
        {
            a[step] = 'o';
            vivod();
        }
    return 0;
}





















int main(void)
{
    int v2 = 0;
    int i =1;
    result = 0;
    a[0]='1';
    a[1]='2';
    a[2]='3';
    a[3]='4';
    a[4]='5';
    a[5]='6';
    a[6]='7';
    a[7]='8';
    a[8]='9';
    v2 = vibor();
    printf("s",v2);
    if (v2 == 1)
    {
        i =1;
        while ((result == 0) && (i<=5))
        {
            krstep();
            result = checkmate_kr('x');
            if (result == 1)
            {
                int z = 0;
                printf("\n");
                printf("Победа крестиков\n");
                printf("Нажмите 1 чтобы вернуться\n");
                scanf("%d",&z);
                if (z == 1)
                {
                    main();
                }
            }
            if (i == 5)
            {
                int z = 0;
                printf("\n");
                printf("Ничья\n");
                printf("Нажмите 1 чтобы вернуться\n");
                scanf("%d",&z);
                if (z == 1)
                {
                    main();
                }
                
                
            }
            nullstep();
            result = checkmate_kr('o');
            
            if (result == 1)
            {
                int z = 0;
                printf("\n");
                printf("Победа ноликов\n\n");
                printf("Нажмите 1 чтобы вернуться\n");
                scanf("%d",&z);
                if (z == 1)
                {
                    main();
                }
                
            }
            i++;
        }
    }
    /*else if (v2==2)
    {
        
    }*/
    if (v2 == 3)
        return 0;     
}
