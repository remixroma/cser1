/************************************************/
/*    server.c - TCP/IP сервер мессенджера      */
/*  Паньшин Роман 26.07.2020                    */
/*  Версия 1.4.1 open chat                      */
/************************************************/

//подключаем необходимые заголовчные файлы
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <netdb.h>
#include <sys/poll.h>
#include <unistd.h>

int extern errno;     //глобальная переменная, которая хранит код последней ошибки

//проверка, задан ли шаблон INADDR_NONE, который обозначает сразу все доступные сетевые интерфейсы
//на некоторых платформах, он может быть не задан.
#ifndef INADDR_NONE
#define INADDR_NONE 0xfffffffff
#endif

#define MESSAGE_SIZE 256 // размер сообщения
#define CLIENT_QUANTITY 50
#define RESET   "\033[0m"
#define YELLOW  "\033[1;33m"
#define RED     "\033[1;31m"
#define GREEN   "\x1B[32m"   //цвета для сообщений

//функция создания и связывания сокета. объявление
//аргументы:
//port - порт, с которым связывается сервер
//transport - протокол, по которому будет работать сервер (tcp или udp)
//qlen - длина  очереди
int sock(const char *port, const char *transport, int qlen);


int sendMessage(int csock, char* message) //функция отправки сообщения
{
    char ans[MESSAGE_SIZE];                    //буфер сообщения
    memset(&ans, 0, sizeof(ans));                   //обнуляем буфер
    strcpy(ans, message);
     if(send(csock, ans, MESSAGE_SIZE,0) < 0)        //отсылаем серверу
    {
        printf("%sНе удалось отправить данные: %s%s\n",RED, strerror(errno),RESET);
        return -1;
    }
    printf("Отправлено %s\n",message);    //читаем ответ сервера
    return 0;
}

char *nameEdditor(char name[MESSAGE_SIZE])  //функция редактирует имя
{
    char sys[MESSAGE_SIZE]; //буфер
    strcpy(sys,"\x1B[36m<"); // красим в голубой
    strcat(sys, name); // добавлием никнейм
    strcat(sys, "> \033[0m"); //закрываем строку и ресетаем цвет
    strcpy(name, sys); //заполняем из буфера в выодимое
    printf("%s\n", name);
    return name;
}

char *nameAdder(char name[MESSAGE_SIZE], char message[MESSAGE_SIZE]) //функция добавляет имя в сообщение
{
    char result[MESSAGE_SIZE];      //буфер
    strcpy(result, name);           //добавляем имя
    strcat(result, message);        //к сообщению
    strcpy(message, result);        //копируем буфер в сообщение
    return message;
}

struct clientsdate
{
    int csock;
    char name[MESSAGE_SIZE];
};

//главная функция
int main(int argc, char **argv)
{
    int msock;                   //дескрипторы сокетов
    struct sockaddr_in  remaddr;                //структура IP-адреса первого клиента
    struct pollfd mainsock;                     //структура для проверки главного сокета
    struct pollfd fds[CLIENT_QUANTITY];                       // структуры для проверки двух клиентов
    struct clientsdate connected[CLIENT_QUANTITY];
    unsigned int remaddrs = sizeof(remaddr);    //размер структуры адреса
    unsigned int clientscount  = 0;
    char msg[MESSAGE_SIZE]; //сообщение, имена клиентов
    char sys[MESSAGE_SIZE]; //консольный вывод
    
    msock = sock("3123", "tcp", 5);    //создаем tcp сокет и привязываем его к порту 3123, задав очередь 5
    if(msock < 0)            //проверяем значение дескриптора сокета
        return -1;        //завершаем программу
    mainsock.fd = msock;
    mainsock.events = POLLIN;
    
    system("clear");
    printf("%sСервер запущен%s\n", GREEN, RESET);
    while(1)    //бесконечный цикл
    {
        int ret = poll(&mainsock, 1, 1);
        if ((mainsock.revents & POLLIN))
        {
            mainsock.revents = 0;
            connected[clientscount].csock = accept(msock, (struct sockaddr*) &remaddr, &remaddrs);    //принимаем входящее подключение, адрес клиента в remaddr
            fds[clientscount].fd = connected[clientscount].csock;
            fds[clientscount].events = POLLIN; // ожидаем входящие данные
            recv(connected[clientscount].csock, &msg, MESSAGE_SIZE, 0); //пинимаем имя от первого пользователя
            strcpy(connected[clientscount].name, nameEdditor(msg)); //заполняем из ф-ии
            memset(msg,'\0', MESSAGE_SIZE); // очищаем msg
            strcpy(sys, "Подключен:  ");
            strcat(sys, inet_ntoa(remaddr.sin_addr)); //добавляем в строку ip клиента
            printf("\n%s%s%s\n", YELLOW, sys, RESET);
            memset(sys, '\0', MESSAGE_SIZE);
            clientscount++;
        }
        if (clientscount > 0) //если все нормально - начинаем обмен данными с клиентом
        {
            ret = poll(fds, clientscount, 1); //проверем извенения дискрипторов
            if ( ret == -1 )
            {
                    printf("%sОшибка проверки сокетов: %s%s", RED, strerror(errno), RESET);
                    return -1;
            }
            else if ( ret == 0 )
                    usleep(1);
            else
            {
                for(int i = 0; i <= (clientscount - 1); i++) //обходим массив структур подкдюченных пользователлей
                {
                    if (fds[i].revents & POLLIN ) //проверяем если от i-го пользователя пришло сообщения
                    {
                        fds[i].revents = 0; //очищаем события
                        recv(connected[i].csock, &msg, MESSAGE_SIZE, 0); //получаем сообщение из сокета
                        if (strncmp(msg, "exit", 4) == 0)
                        {
                            sendMessage(connected[i].csock, "exit");
                            shutdown(connected[i].csock, 2);        //закрываем сокет сервера
                            for(int j = i; j < clientscount-1; j++)
                            {
                                connected[j] = connected[j+1];
                                fds[j] = fds[j+1];
                            }
                            clientscount = clientscount - 1;
                        }
                        else
                        {
                            printf("От клиента %d получено: %s\n",i, msg);
                            strcpy(msg, nameAdder(connected[i].name, msg));
                            for(int j = 0; j <= (clientscount-1); j++)
                            {
                                if(j!=i)
                                    sendMessage(connected[j].csock, msg);         //отправляем прочитанное
                            }
                            memset(&msg, '\0', MESSAGE_SIZE);     //очищаем
                        }
                    }
                }
            }
        }
    }
    return 0;
}
//функция создания и связывания сокета. реализация
int sock(const char *port, const char *transport, int qlen)
{
    struct protoent *ppe;
    struct sockaddr_in sin;
    int s, type;
    //обнуляем структуру адреса
    memset(&sin, 0, sizeof(sin));
    //указываем тип адреса - IPv4, для IPv6 необходимо указать AF_INET6
    sin.sin_family = AF_INET;
    //указываем, в качестве адреса, шаблон INADDR_ANY - все сетевые интерфейсы
    sin.sin_addr.s_addr = INADDR_ANY;
    //задаем порт
    sin.sin_port = htons((unsigned short)atoi(port));
    //преобразовываем имя транспортного протокола в номер протокола
    if((ppe = getprotobyname(transport)) == 0)
        {
            printf("%sОшибка преобразования имени транспортного протокола: %s%s\n",RED, strerror(errno), RESET);    //в случае неудачи выводим сообщение ошибки
            return -1;
        }
    //используем имя протокола для определения типа сокета
    if(strcmp(transport, "udp") == 0)
        type = SOCK_DGRAM;
    else
        type = SOCK_STREAM;
    //создаем сокет
    s = socket(PF_INET, type, ppe->p_proto);
    if(s < 0)
        {
            printf("%sОшибка создания сокета: %s%s\n",RED, strerror(errno), RESET);    //в случае неудачи выводим сообщение ошибки
            return -1;
        }
    //привязка сокета с проверкой результата
    if(bind(s, (struct sockaddr *)&sin, sizeof(sin)) < 0)
        {
            printf("%sОшибка связывания сокета: %s%s\n",RED, strerror(errno), RESET);    //в случае неудачи выводим сообщение ошибки
            return -1;
        }
    //запуск прослушивания с проверкой результата
    if(type == SOCK_STREAM && listen(s, qlen) <0)
        {
            printf("%sОшибка прослушивания сокета: %s%s\n",RED, strerror(errno), RESET);    //в случае неудачи выводим сообщение ошибки
            return -1;
        }
    return s;    //возвращаем дескриптор сокета
}



