/************************************************/
/*	server.c - TCP/IP сервер мессенджера    	*/
/*  Паньшин Роман 24.06.2020                    */
/*  Версия 1.3.1   color update                 */
/************************************************/

//подключаем необходимые заголовчные файлы
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <netdb.h>

extern errno; 	//глобальная переменная, которая хранит код последней ошибки

//проверка, задан ли шаблон INADDR_NONE, который обозначает сразу все доступные сетевые интерфейсы
//на некоторых платформах, он может быть не задан.
#ifndef INADDR_NONE
#define INADDR_NONE 0xfffffffff
#endif

const int MESSAGE_SIZE=21; // размер сообщения
//функция создания и связывания сокета. объявление
//аргументы:
//port - порт, с которым связывается сервер
//transport - протокол, по которому будет работать сервер (tcp или udp)
//qlen - длина  очереди
int sock(const char *port, const char *transport, int qlen);

#define RESET   "\033[0m"
#define YELLOW  "\033[1;33m"
#define RED     "\033[1;31m"
#define GREEN   "\x1B[32m"   //цвета для сообщений

int sendMessage(int csock, char* message) //функция отправки 
{
	char ans[MESSAGE_SIZE];					//буфер сообщения
	memset(&ans, 0, sizeof(ans));                   //обнуляем буфер
    strcpy(ans, message);
 	if(write(csock, ans, sizeof(ans)) < 0)		//отсылаем серверу
	{
		printf("%sНе удалось отправить данные: %s%s\n",RED, strerror(errno),RESET);
		return -1;
	} 
	printf("Отправлено %s\n",message);	//читаем ответ сервера
	return 0;	                                               
}


//главная функция
int main()
{
	int msock, csock, csock2;				//дескрипторы сокетов
	struct sockaddr_in  remaddr;
	struct sockaddr_in  remaddr2;			//структура IP-адреса клиента
	unsigned int remaddrs = sizeof(remaddr);	//размер структуры адреса
	char msg[MESSAGE_SIZE], firstname[MESSAGE_SIZE], secondname[MESSAGE_SIZE]; //сообщение, имена клиентов
	
	msock = sock("3123", "tcp", 5);	//создаем tcp сокет и привязываем его к порту 3123, задав очередь 5
	if(msock < 0)			//проверяем значение дескриптора сокета
		return -1;		//завершаем программу
	
	printf("%s Сервер запущен%s\n",GREEN, RESET);
	while(1)	//бесконечный цикл
	{
		csock = accept(msock, (struct sockaddr*) &remaddr, &remaddrs);	//принимаем входящее подключение, адрес клиента в remaddr
		char sys[MESSAGE_SIZE]; //консольный вывод
        strcpy(sys, "Подключен:  ");
        strcat(sys, inet_ntoa(remaddr.sin_addr));
        printf("\n%s%s%s\n",YELLOW,sys, RESET);
		memset(sys,'\0',MESSAGE_SIZE);
		
		csock2 = accept(msock, (struct sockaddr*) &remaddr2, &remaddrs);
        strcpy(sys, "Подключен:  ");
        strcat(sys, inet_ntoa(remaddr2.sin_addr));
        printf("\n%s%s%s\n",YELLOW, sys, RESET);
		memset(sys,'\0',MESSAGE_SIZE);
        
		if((csock < 0) && (csock2<0))		//проверяем результат
			printf("Ошибка принятия подключения: %s\n", strerror(errno)); //сообщение об ошибке
		else			//если все нормально - начинаем обмен данными с клиентом
			{
				read(csock, &firstname, sizeof(firstname));
				strcat(firstname, ": ");
				printf("%s\n",firstname);
				
				read(csock2, &secondname, sizeof(secondname));
				strcat(secondname, ": ");
				printf("%s\n",secondname);
				
				while(1)		//пробуем читать данные от клиента
				{
					if (read(csock, &msg, sizeof(msg)))
					{ 
						printf("От клиента 1 получено: %s\n", msg); 	
						strcpy(sys, firstname); //добавляем имя 
						strcat(sys, msg);	//к сообщению
						sendMessage(csock2, sys); //отправляем прочитанное
						memset(&sys, 0, sizeof(sys));	 //очищаем
						memset(&msg, 0, sizeof(msg));
					}
					
					if (read(csock2, &msg, sizeof(msg)))
					{
						printf("От клиента 2 получено: %s\n", msg);
						strcpy(sys, secondname); //по аналогии с предыдущим  
						strcat(sys, msg);	
						sendMessage(csock, sys);
						memset(&sys, 0, sizeof(sys));
						memset(&msg, 0, sizeof(msg));
					}
				}
				close(csock);		//закрываем сокет клиента
				close(csock2);
			}
	}	
	close(msock);		//закрываем сокет сервера
	return 0;
}
//функция создания и связывания сокета. реализация
int sock(const char *port, const char *transport, int qlen)
{
	struct protoent *ppe;		
	struct sockaddr_in sin;
	int s, type;
	//обнуляем структуру адреса
	memset(&sin, 0, sizeof(sin));
	//указываем тип адреса - IPv4, для IPv6 необходимо указать AF_INET6
	sin.sin_family = AF_INET;
	//указываем, в качестве адреса, шаблон INADDR_ANY - все сетевые интерфейсы
	sin.sin_addr.s_addr = INADDR_ANY;
	//задаем порт
	sin.sin_port = htons((unsigned short)atoi(port));
	//преобразовываем имя транспортного протокола в номер протокола
	if((ppe = getprotobyname(transport)) == 0)
		{
			printf("%sОшибка преобразования имени транспортного протокола: %s%s\n",RED, strerror(errno), RESET);	//в случае неудачи выводим сообщение ошибки
			return -1;		
		}
	//используем имя протокола для определения типа сокета 	 
	if(strcmp(transport, "udp") == 0)
		type = SOCK_DGRAM;
	else
		type = SOCK_STREAM;	
	//создаем сокет
	s = socket(PF_INET, type, ppe->p_proto);
	if(s < 0)
		{
			printf("%sОшибка создания сокета: %s%s\n",RED, strerror(errno), RESET);	//в случае неудачи выводим сообщение ошибки
			return -1;
		}
	//привязка сокета с проверкой результата
	if(bind(s, (struct sockaddr *)&sin, sizeof(sin)) < 0)
		{
			printf("%sОшибка связывания сокета: %s%s\n",RED, strerror(errno), RESET);	//в случае неудачи выводим сообщение ошибки
			return -1;
		}
	//запуск прослушивания с проверкой результата
	if(type == SOCK_STREAM && listen(s, qlen) <0)
		{
			printf("%sОшибка прослушивания сокета: %s%s\n",RED, strerror(errno), RESET);	//в случае неудачи выводим сообщение ошибки
			return -1;
		}
	return s;	//возвращаем дескриптор сокета
}



